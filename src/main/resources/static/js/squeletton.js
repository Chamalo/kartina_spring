function displayAriane() {
    var ariane = document.getElementById("filAriane");
    var main = document.getElementsByTagName("main");

    if (document.title == "Accueil") {
        ariane.style.display = "none";
        main[0].style.minHeight = "calc( 100vh - 232px )"; //Permet de fixer le footer en bas de la page
    } else {
        ariane.style.display = "block"
        main[0].style.minHeight = "calc( 100vh - 262px )"; //Permet de fixer le footer en bas de la page
    }
}