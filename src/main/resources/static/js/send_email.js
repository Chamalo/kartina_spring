function validateForm(formulaire) {
    if(formulaire.civ1.checked || formulaire.civ2.checked || formulaire.civ3.checked) {
        if(formulaire.firstname.value.length != 0 && formulaire.lastname.value.length != 0 && formulaire.mail.value.length != 0 && formulaire.message.value.length != 0) {
            formulaire.sendBtn.disabled = false;
        } else {
            formulaire.sendBtn.disabled = true;
        }
    } else {
        formulaire.sendBtn.disabled = true;
    }
}

function validateField(formulaire) {
    if(formulaire.value.length == 0) {
        document.getElementById(formulaire.id + "SpanError").innerHTML = formulaire.id + " dois etre remplis";
    } else {
        document.getElementById(formulaire.id + "SpanError").innerHTML = "";
    }
}