package com.kartina.spring.services;

import com.kartina.spring.models.ContactModel;
import com.kartina.spring.repositories.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactService {

    private final ContactRepository contactRepository;

    @Autowired
    public ContactService(ContactRepository contactRepository) { this.contactRepository = contactRepository; }

    public void save(ContactModel contactModel) { contactRepository.save(contactModel); }
}
