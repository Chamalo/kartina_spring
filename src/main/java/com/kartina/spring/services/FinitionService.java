package com.kartina.spring.services;

import java.util.Collection;
import java.util.Optional;

import com.kartina.spring.models.Finition;
import com.kartina.spring.repositories.FinitionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FinitionService {
    

    private FinitionRepository finitionRepository;

    @Autowired
        public FinitionService(FinitionRepository finitionRepository){
            this.finitionRepository = finitionRepository;
        }
        
        public Collection<Finition> findAll(){
            return finitionRepository.findAll();
        }
    
        public void delete(String type_finition) {
            finitionRepository.deleteById(type_finition);
        }
        
    
        public Optional<Finition> findById(String type_finition) {
            return finitionRepository.findById(type_finition);
        }
    
        public void save(Finition finition) {
            finitionRepository.save(finition);
        }
}
