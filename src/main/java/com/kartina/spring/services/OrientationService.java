package com.kartina.spring.services;

import java.util.Collection;
import java.util.Optional;

import com.kartina.spring.models.Orientation;
import com.kartina.spring.repositories.OrientationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrientationService {
    

    private OrientationRepository orientationRepository;

    
@Autowired
public OrientationService (OrientationRepository orientationRepository){
    this.orientationRepository = orientationRepository;
}

public Collection<Orientation> findAll(){
    return orientationRepository.findAll();
}

public void delete(String type_orientation) {
    orientationRepository.deleteById(type_orientation);
}


public Optional<Orientation> findById(String type_orientation) {
    return orientationRepository.findById(type_orientation);
}

public void save(Orientation orientation) {
    orientationRepository.save(orientation);
}
}
