package com.kartina.spring.services;

import java.util.List;
import java.util.Optional;

import com.kartina.spring.models.Oeuvre;
import com.kartina.spring.repositories.OeuvreRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OeuvreService {

    private final OeuvreRepository oeuvreRepository;

    @Autowired
    public OeuvreService(OeuvreRepository oeuvreRepository) {
        this.oeuvreRepository = oeuvreRepository;
    }

    public Iterable<Oeuvre> findAll() {
        return oeuvreRepository.findAll();
    }

    public void delete(String titre) {
        oeuvreRepository.deleteById(titre);
    }

    public Optional<Oeuvre> findById(String titre) {
        return oeuvreRepository.findById(titre);
    }

    public void save(Oeuvre oeuvre) {
        oeuvreRepository.save(oeuvre);
    }

    public List<Oeuvre> caroussel() {
        return oeuvreRepository.findOeuvresByOrderByDateAjoutDesc();
    }

    public List<Oeuvre> quatro() {
        return oeuvreRepository.quatro();

    }

    public List<Oeuvre> biogra() {
        return oeuvreRepository.biogra();
    }

    public List<Oeuvre> listoeuvres() {
        return oeuvreRepository.listoeuvres();
    }

    public List<Oeuvre> promo() {
        return oeuvreRepository.promo();
    }

    public Iterable<Oeuvre> photographiartistByNameArtiste(String nameArtiste) {
        return oeuvreRepository.findAll();
    }

    public Iterable<Oeuvre> promoByNameArtiste(String firstname) {
        return oeuvreRepository.findOeuvresByOrderByDateAjoutDesc();
    }

    public List<Oeuvre> searchphotography() {
        return oeuvreRepository.searchphotography();
    }

    /* public List<Oeuvre> newphotography() {
        return oeuvreRepository.newphotography();
    } */

    public List<Oeuvre> lastphotography() {
        return oeuvreRepository.lastphotography();
    }

	public List<Oeuvre> newphotography() {
        return oeuvreRepository.findOeuvresByOrderByDateAjoutDesc();
    }

	public void delete(Long ref) {
	}

	public Optional<Oeuvre> findById(Long ref) {
		return null;
	}
    
}