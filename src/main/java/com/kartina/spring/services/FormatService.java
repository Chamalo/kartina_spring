package com.kartina.spring.services;

import java.util.Collection;
import java.util.Optional;

import com.kartina.spring.models.Format;

import com.kartina.spring.repositories.FormatRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FormatService {
    

    private FormatRepository formatRepository;
    

    @Autowired
    public FormatService(FormatRepository formatRepository) {
        this.formatRepository = formatRepository;
    }

    public Collection<Format> findAll() {
        return formatRepository.findAll();
    }

    public void delete(String type_format) {
        formatRepository.deleteById(type_format);
    }

    public Optional<Format> findById(String id) {
        return formatRepository.findById(id);
    }

    public void save(Format format) {
        formatRepository.save(format);
    }  
}
