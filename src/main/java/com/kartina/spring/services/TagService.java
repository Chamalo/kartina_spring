package com.kartina.spring.services;

import java.util.Collection;
import java.util.Optional;

import com.kartina.spring.models.Tag;
import com.kartina.spring.repositories.TagRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TagService {
    

    private TagRepository tagRepository;

@Autowired
    public TagService(TagRepository tagRepository){
        this.tagRepository = tagRepository;
    }
    
    public Collection<Tag> findAll(){
        return tagRepository.findAll();
    }

	public void delete(String tag) {
        tagRepository.deleteById(tag);
    }
    

	public Optional<Tag> findById(String tag) {
		return tagRepository.findById(tag);
	}

	public void save(Tag tag) {
        tagRepository.save(tag);
	}
}
