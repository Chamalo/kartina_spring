package com.kartina.spring.services;

import java.util.Optional;

import com.kartina.spring.models.User;
import com.kartina.spring.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void save(User user) {
        userRepository.save(user);

    }

    public Iterable<User> findAll(){
        return this.userRepository.findAll();
    }

    public Optional<User> findByEmail(String email) {
        return this.userRepository.findById(email);
    }

    public void delete(String email) {
        userRepository.deleteById(email);
    }

}
