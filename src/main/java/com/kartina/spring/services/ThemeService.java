package com.kartina.spring.services;

import java.util.Collection;
import java.util.Optional;

import com.kartina.spring.models.Theme;
import com.kartina.spring.repositories.OeuvreRepository;
import com.kartina.spring.repositories.ThemeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ThemeService {

    private ThemeRepository themeRepository;
    private OeuvreRepository oeuvreRepository;

@Autowired
    public ThemeService(ThemeRepository themeRepository, OeuvreRepository oeuvreRepository){
        this.themeRepository = themeRepository;
        this.oeuvreRepository = oeuvreRepository;
    }
    
    public Collection<Theme> findAll(){
        return themeRepository.findAll();
    }

	public void delete(String typeTheme) {
        themeRepository.deleteById(typeTheme);
    }
    

	public Optional<Theme> findById(String typeTheme) {
		return themeRepository.findById(typeTheme);
	}

	public void save(Theme theme) {
        themeRepository.save(theme);
    }

	public Collection<Theme> findByTypeTheme(String typeTheme) {
		return this.themeRepository.findByTypeTheme(typeTheme);
    }
    
    
    

    
    
}
