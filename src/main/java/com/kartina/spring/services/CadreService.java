package com.kartina.spring.services;

import java.util.Collection;
import java.util.Optional;

import com.kartina.spring.models.Cadre;
import com.kartina.spring.repositories.CadreRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CadreService {
    

    private CadreRepository cadreRepository;

    @Autowired
        public CadreService(CadreRepository cadreRepository){
            this.cadreRepository = cadreRepository;
        }
        
        public Collection<Cadre> findAll(){
            return cadreRepository.findAll();
        }
    
        public void delete(String type_cadre) {
            cadreRepository.deleteById(type_cadre);
        }
        
    
        public Optional<Cadre> findById(String type_cadre) {
            return cadreRepository.findById(type_cadre);
        }
    
        public void save(Cadre cadre) {
            cadreRepository.save(cadre);
        }
}
