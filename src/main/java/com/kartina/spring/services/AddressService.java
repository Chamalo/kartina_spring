package com.kartina.spring.services;

import java.util.Collection;
import java.util.Optional;

import com.kartina.spring.models.Address;
import com.kartina.spring.models.User;
import com.kartina.spring.repositories.AddressRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressService {

	private AddressRepository adresseRepository;

	@Autowired
	public AddressService(AddressRepository adresseRepository) {
		this.adresseRepository = adresseRepository;
	}

	public void save(Address adresse) {
		adresseRepository.save(adresse);
	}

	public Optional<Address> findById(Long Id) {
		return this.adresseRepository.findById(Id);
	}

	public Collection<Address> findByUsers(Collection<User> users) {
		return this.adresseRepository.findAllByUserIn(users);
	}

	public void delete(Long Id) {
		adresseRepository.deleteById(Id);
	}

	public Collection<Address> findAll() {
		return (Collection<Address>) adresseRepository.findAll();

	}
}