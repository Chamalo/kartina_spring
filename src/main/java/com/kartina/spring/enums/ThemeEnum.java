package com.kartina.spring.enums;

public enum ThemeEnum {


    Femme ("femme"),
    Nature ("nature"),
    Noir_blanc ("noir et blanc"),
    Paysage ("paysage"),
    Urban ("urban"),
    Vintage ("vintage"),
    Voyage ("voyage");

    private String typeTheme;

	private ThemeEnum(String type) {
		this.typeTheme = type;
	}

	public String getType() {
		return typeTheme;
	}                          
    
}
