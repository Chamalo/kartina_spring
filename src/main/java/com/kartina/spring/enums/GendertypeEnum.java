package com.kartina.spring.enums;

public enum GendertypeEnum {
    
	MLLE  ("Mlle"),
	MME ("Mme"),
    MR ("Mr");			

	private String Gtype;

	private GendertypeEnum(String Gtype) {
		this.Gtype = Gtype;
	}

	public String getType() {
		return Gtype;
	}
 
}

