package com.kartina.spring.enums;

public enum SocialNetworkEnum {
    
    FACEBOOK ("Facebook"),					
	TWITTER ("Twitter"),
	SNAPCHAT ("Snapchat"),
	PINTEREST ("Pinterest"),
	LINKEDLN ("Linkedln"),
	INSTAGRAM ("Instagram"),
	FLICKR	("Flickr"),
	WEIBO	("Weibo"),
	YOUTUBE ("YouTube"),
	TIKTOK ("TikTok");

	private String SNtype;

	private SocialNetworkEnum(String SNtype) {
		this.SNtype = SNtype;
	}

	public String getType() {
		return SNtype;
	}
 
}
