package com.kartina.spring.enums;

public enum AddresstypeEnum {
    
    RUE ("Rue"),					
	BOULEVARD ("Bvd"),
	IMPASSE  ("Impasse");

	private String type;

	private AddresstypeEnum(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
 
}
