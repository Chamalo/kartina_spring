package com.kartina.spring.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Cadre {
    

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
        
        private String type_cadre;
    
        @Column(nullable = false)
        private double price;

        @ManyToMany
        private List<Oeuvre> oeuvres = new ArrayList<>();
}
