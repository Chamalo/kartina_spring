package com.kartina.spring.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Theme {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name= "type_theme")
    private String typeTheme;

    @Column(nullable = false)
    private double price;

    @ManyToMany
    @ToString.Exclude List<Oeuvre> oeuvres = new ArrayList<>();

}
