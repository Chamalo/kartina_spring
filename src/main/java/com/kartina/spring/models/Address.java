package com.kartina.spring.models;

import java.util.Collection;

import javax.persistence.*;

import com.kartina.spring.enums.AddresstypeEnum;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "address")
public class Address {

	@Id				
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private Long id;

    private Integer num;
    @Column(length=20)
	@Enumerated()
	private AddresstypeEnum type;
	private String street;
    private String complement;
    private String zipCode;
	@Column(nullable = false)
	private String city;
	@Column(nullable = false,length=30)
	private String country;
	@Column(length=10)
	private String addressName;
	
	
	@ManyToMany(mappedBy = "adresses")
	@ToString.Exclude private Collection<User> user;
}