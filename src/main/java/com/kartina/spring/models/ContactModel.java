package com.kartina.spring.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "contact")
public class ContactModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 5, nullable = false)
    @NotBlank
    private String civ;

    @Column(length = 155, nullable = false)
    @NotBlank
    private String lastname;

    @Column(length = 155, nullable = false)
    @NotBlank
    private String firstname;

    @Column(nullable = false)
    @Email
    @NotBlank
    @Pattern(regexp = "[^@ \\t\\r\\n]+@[^@ \\t\\r\\n]+.[^@ \\t\\r\\n]+", message = "It's not an email !")
    private String mail;

    @Pattern(regexp = "^\\s*|^0[1-68]([-. ]?[0-9]{2}){4}$", message = "It's not a phone number !")
    private String phone;

    @Column(length = 40, nullable = false)
    @NotBlank
    private String subject;

    @Column(columnDefinition = "TEXT", nullable = false)
    @NotBlank
    private String message;
}
