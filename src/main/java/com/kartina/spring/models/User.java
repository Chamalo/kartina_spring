package com.kartina.spring.models;

import java.util.Collection;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

import com.kartina.spring.enums.GendertypeEnum;
import com.kartina.spring.enums.RoleEnum;
import com.kartina.spring.enums.SocialNetworkEnum;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user")
public class User {

    @Id			
    @Column(length = 150)
    private String email;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private GendertypeEnum Gtype;
    
	@Column(nullable = false)
	private String lastname;
	
	private String firstname;
	
    private Integer phone;
    
    @Transient
    @Pattern(regexp = "^.{6,150}$", flags = {Pattern.Flag.CASE_INSENSITIVE, Pattern.Flag.DOTALL})
    private String pwd;
    
    @Column(length = 150, nullable = false)
    private String pwdHash;
	
	@Column(length=128)
    private String salt;
    
    private Integer session_id;

    private Integer registration_date;

    private String biographie;

    @Enumerated(EnumType.STRING)
    private SocialNetworkEnum SNtype;

    @Enumerated(EnumType.STRING)
    private RoleEnum role;


    @ManyToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL)
	@ToString.Exclude private Collection<Address> adresses;
	
    @ManyToOne
    private Usertype userType;
    
    
}