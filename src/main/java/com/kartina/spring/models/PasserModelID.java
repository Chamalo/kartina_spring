package com.kartina.spring.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class PasserModelID implements Serializable {

    private Long commandeId;

    private Long oeuvreId;

    private String userId;
}