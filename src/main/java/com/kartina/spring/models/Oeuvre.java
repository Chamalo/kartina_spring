package com.kartina.spring.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.NoArgsConstructor;

 
@Data
@NoArgsConstructor
@AllArgsConstructor


@Entity
public class Oeuvre{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(nullable = false)
    @NotNull
    @NotBlank
    @Pattern(
        regexp = "^[a-z 0-9.\\-_\\\\/À-ÖØ-öø-ÿ]{3,255}$", 
        flags = {Pattern.Flag.CASE_INSENSITIVE, Pattern.Flag.DOTALL})
    @NonNull
    private String nameArtiste;

    @Column(columnDefinition = "TEXT", length = 2000)
    private String link;

    @Column(nullable = false)
    private double price;

    @NonNull
    @Column(columnDefinition = "TEXT")
    private String tirage;

    @Column(columnDefinition = "DATE", nullable = false)
    private String dateAjout;

 
    @ManyToMany(mappedBy = "oeuvres")
    private List<Theme> themes = new ArrayList<>();


	public Object getTypeTheme() {
		return null;
	} 

}