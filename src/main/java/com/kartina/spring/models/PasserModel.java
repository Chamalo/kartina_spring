package com.kartina.spring.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PasserModel{

    @EmbeddedId
    private PasserModelID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("commandeId")
    private CommandeModel commande;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("oeuvreId")
    private Oeuvre oeuvre;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    private User user;

    private String format;

    private String cadre;

    private String finition;

    private String titre;
}