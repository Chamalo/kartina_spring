package com.kartina.spring.models;

import java.util.Collection;

import javax.persistence.*;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_type")
public class Usertype {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)				
    private Long code;

    @Column(nullable = false)
    private String rang;

    @OneToMany(mappedBy = "userType")
    private Collection<User> users;
    
}