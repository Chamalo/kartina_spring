package com.kartina.spring.models;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "commande")
public class CommandeModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Double prix_total;

    @Column(nullable = false)
    private Integer quantite_total;

    @Column(columnDefinition = "DATE")
    private String date_livraison;

    @Column(columnDefinition = "DATE", nullable = false)
    private String date_commande;

    @OneToMany(mappedBy = "commande")
    private Collection<PasserModel> passerModels;
}
