package com.kartina.spring.repositories;

import com.kartina.spring.models.Finition;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FinitionRepository extends JpaRepository<Finition, String> {
    
}
