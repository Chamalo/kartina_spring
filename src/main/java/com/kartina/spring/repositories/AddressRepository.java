package com.kartina.spring.repositories;

import java.util.Collection;

import com.kartina.spring.models.Address;
import com.kartina.spring.models.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
 
    public interface AddressRepository extends CrudRepository <Address, Long> {
        Collection<Address> findAllByUserIn(Collection<User> users);
}
