package com.kartina.spring.repositories;

import com.kartina.spring.models.Format;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormatRepository extends JpaRepository<Format, String> {
    
}
