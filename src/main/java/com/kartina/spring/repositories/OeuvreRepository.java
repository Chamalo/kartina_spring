package com.kartina.spring.repositories;

import java.util.List;
import java.util.Optional;

import com.kartina.spring.models.Oeuvre;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface OeuvreRepository extends CrudRepository<Oeuvre, String>{

    List<Oeuvre> findOeuvresByOrderByDateAjoutDesc();


    @Query(value = "select * from oeuvre order by date_ajout limit 4", nativeQuery = true)
    List<Oeuvre> quatro();
   

   @Query(value = "select * from oeuvre order by tirage limit 8", nativeQuery = true)
   List<Oeuvre> listoeuvres();

   @Query(value = "select * from oeuvre order by tirage limit 1", nativeQuery = true)
   List<Oeuvre> biogra();

   @Query(value = "select * from oeuvre order by date_ajout limit 4", nativeQuery = true)
   List<Oeuvre> promo();



   @Query(value = "select * from oeuvre order by link limit 24", nativeQuery = true)
    List<Oeuvre> searchphotography();

    @Query(value = "select * from oeuvre order by date_ajout limit 24", nativeQuery = true)
    List<Oeuvre> newphotography();


   /*  List<Oeuvre> findOeuvresByOrderByDateAjoutDesc(); */

    //List<Oeuvre> findOeuvresByOrderByDateAjoutAsc();

    @Query(value = "select * from oeuvre limit 95, 130", nativeQuery = true)
    List<Oeuvre> lastphotography();


	Optional<Oeuvre> findById(Long ref);


}