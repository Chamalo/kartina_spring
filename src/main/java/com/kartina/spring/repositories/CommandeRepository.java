package com.kartina.spring.repositories;

import com.kartina.spring.models.CommandeModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommandeRepository extends CrudRepository<CommandeModel, Long> {
}
