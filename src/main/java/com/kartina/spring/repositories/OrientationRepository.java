package com.kartina.spring.repositories;

import com.kartina.spring.models.Orientation;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrientationRepository extends JpaRepository<Orientation, String> {
    
}
