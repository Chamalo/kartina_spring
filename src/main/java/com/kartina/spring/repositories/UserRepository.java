package com.kartina.spring.repositories;

import java.util.List;
import java.util.Optional;

import com.kartina.spring.models.User;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
 
    public interface UserRepository extends CrudRepository <User, String> {


        @Query(value = "SELECT * FROM user WHERE email = ?1", nativeQuery = true)
        Optional<User> findByEmail(String email);
      
        @Query(value = "select * from oeuvre where name_artiste from order by date_ajout limit 80", nativeQuery = true)
        <Oeuvre> List<Oeuvre> promo();

        
}
