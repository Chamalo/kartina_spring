package com.kartina.spring.repositories;

import com.kartina.spring.models.Cadre;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CadreRepository extends JpaRepository<Cadre, String> {
    
}
