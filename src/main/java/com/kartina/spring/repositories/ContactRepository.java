package com.kartina.spring.repositories;

import com.kartina.spring.models.ContactModel;
import org.springframework.data.repository.CrudRepository;

public interface ContactRepository extends CrudRepository<ContactModel, Long> {
}
