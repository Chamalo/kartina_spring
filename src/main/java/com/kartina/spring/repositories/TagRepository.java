package com.kartina.spring.repositories;

import com.kartina.spring.models.Tag;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, String> {
    
}
