package com.kartina.spring.repositories;

import com.kartina.spring.models.FactureModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FactureRepository extends CrudRepository<FactureModel, Long> {
}
