package com.kartina.spring.repositories;

import com.kartina.spring.models.Usertype;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
 
    public interface UsertypeRepository extends CrudRepository <Usertype, Long> {
    
}
