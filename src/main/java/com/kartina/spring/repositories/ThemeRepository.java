package com.kartina.spring.repositories;

import java.util.Collection;

import com.kartina.spring.models.Theme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface ThemeRepository extends JpaRepository<Theme, String> {

    Collection<Theme> findByTypeTheme(String typeTheme);

    
}
