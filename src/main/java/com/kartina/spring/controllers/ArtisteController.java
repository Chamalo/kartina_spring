package com.kartina.spring.controllers;

import com.kartina.spring.services.OeuvreService;
import com.kartina.spring.services.UserService;

import java.util.Optional;

import com.kartina.spring.models.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/artistes")
public class ArtisteController {
    
    private final OeuvreService oeuvreService;
    private final UserService userService;

    @Autowired
    public ArtisteController(OeuvreService oeuvreService, UserService userService){
        this.oeuvreService = oeuvreService;
        this.userService = userService;
    }


    @GetMapping("")
    public String artiste(
        Model model,
        @RequestParam(defaultValue = "") String email) {
        Optional<User> user = this.userService.findByEmail(email);
        if(user.isPresent()) { 
            model.addAttribute("artiste", user.get()); }
        model.addAttribute("oeuvresz", oeuvreService.photographiartistByNameArtiste(user.get().getFirstname()));
        model.addAttribute("oeuvrepromo", oeuvreService.promoByNameArtiste(user.get().getFirstname()));
        model.addAttribute("fragment", "artistes");
        return "index";
    }  
}
