package com.kartina.spring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import com.kartina.spring.enums.GendertypeEnum;
import com.kartina.spring.enums.RoleEnum;
import com.kartina.spring.models.Address;
import com.kartina.spring.models.User;
import com.kartina.spring.services.AddressService;
import com.kartina.spring.services.UserService;

@Controller
public class AccountController {

    private final UserService userService;
    private final AddressService adresseService;

    @Autowired
    public AccountController(UserService userService, AddressService adresseService) {
        this.userService = userService;
        this.adresseService = adresseService;
    }

    @GetMapping("/moncompte")
    public String moncompte(Model model, User user) {
        model.addAttribute("user", user);
        model.addAttribute("fragment", "compteutilisateur");
        return "index";
    }

    @GetMapping("/donneespersonnelles")
    public String mesdonnees(Model model, HttpSession session) {
        model.addAttribute("user", (User) session.getAttribute("user"));
        model.addAttribute("fragment", "donneespersonnelles");
        model.addAttribute("GtypeEnumList", GendertypeEnum.values());
        return "index";
    }

    @PostMapping("/donneespersonnelles")
    public String mesdonnees(Model model, @Valid @ModelAttribute(name = "user") User user, HttpSession session,
            BindingResult userBinding) {

        if (!userBinding.hasErrors()) {

            User userSession = (User) session.getAttribute("user");
            userSession.setGtype(user.getGtype());
            userSession.setFirstname(user.getFirstname());
            userSession.setLastname(user.getLastname());
            userSession.setPhone(user.getPhone());
            this.userService.save(userSession);
            session.setAttribute("user", userSession);
            return "redirect:/";

        }
        model.addAttribute("user", (User) session.getAttribute("user"));
        model.addAttribute("fragment", "donneespersonnelles");
        model.addAttribute("gTypeEnumList", GendertypeEnum.values());
        return "index";
    }

    @GetMapping("/tableauadresseslivraison")
    public String mesadresses(Model model, HttpSession session) {
        model.addAttribute("adresses", ((User) session.getAttribute("user")).getAdresses());
        model.addAttribute("fragment", "tableauadresseslivraison");
        return "index";
    }

    @GetMapping("/tableauadresseslivraison/{id}")
    public String deletemesadresses(@PathVariable Long id, HttpSession session) {
        User user = (User) session.getAttribute("user");
        user.setAdresses(user.getAdresses().stream().filter(ad -> !ad.getId().equals(id)).collect(Collectors.toList()));
        userService.save(user);
        adresseService.delete(id);
        return "redirect:/tableauadresseslivraison";
    }

    @GetMapping("/tableauutilisateurs")
    public String mesutilisateurs(Model model) {
        model.addAttribute("users", userService.findAll());
        model.addAttribute("fragment", "tableauutilisateurs");
        return "index";
    }

    @GetMapping("/tableauutilisateurs/{email}")
    public String deleteuser(@PathVariable String email, HttpSession session) {
        userService.delete(email);
        return "redirect:/tableauutilisateurs";
    }

    @GetMapping("/donneespersonnellesadmin/{email}")
    public String mesdonneesadmin(Model model, HttpSession session, @PathVariable String email) {
        Optional<User> user = userService.findByEmail(email);
        if (user.isPresent()){
            model.addAttribute("user", user.get());
            model.addAttribute("fragment", "donneespersonnellesadmin");
            return "index";
        }
        return "redirect:/tableauutilisateurs";
    }



    @PostMapping("/donneespersonnellesadmin/{email}")
    public String updateuser(Model model,
            @PathVariable String email,
            @Valid @ModelAttribute(name = "user") User user,
            BindingResult userBinding,
            HttpSession session) {

        if (!userBinding.hasErrors()) {
            Optional<User> userBdd = userService.findByEmail(email);
            if (userBdd.isPresent()){
                userBdd.get().setRole(user.getRole());
                this.userService.save(userBdd.get());
                return "redirect:/tableauutilisateurs";
            }
        }
        model.addAttribute("user", user);
        model.addAttribute("fragment", "donneespersonnellesadmin");
        return "index";
    }

    @GetMapping("/nouvelleadresse")
    public String nouvelleadresse(Model model, HttpSession session) {
        model.addAttribute("user", (User) session.getAttribute("user"));
        model.addAttribute("adresse", new Address());
        model.addAttribute("fragment", "nouvelleadresse");
        return "index";
    }

    @PostMapping("/nouvelleadresse")
    public String nouvelleadresse(@Valid @ModelAttribute(name = "adresse") Address adresse, HttpSession session) {
        this.adresseService.save(adresse);
        User user = (User) session.getAttribute("user");
        user.getAdresses().add(adresse);
        userService.save(user);
        session.setAttribute("user", user);
        return "redirect:/tableauadresseslivraison";
    }

    @GetMapping("/adresseslivraison/{id}")
    public String adresseUpdate(Model model, HttpSession session, @PathVariable Long id) {
        User user = (User) session.getAttribute("user");
        if (user.getAdresses().size() > 0) {
            model.addAttribute("adresse",
                    user.getAdresses().stream().filter(ad -> ad.getId().equals(id)).findFirst().get());
        }
        model.addAttribute("fragment", "adresseslivraison");
        return "index";
    }

    @PostMapping("/adresseslivraison")
    public String mesadresses(@Valid @ModelAttribute(name = "adresse") Address adresse, HttpSession session) {
        this.adresseService.save(adresse);
        User user = (User) session.getAttribute("user");
        if (adresse.getId() == null) {
            user.getAdresses().add(adresse);
            userService.save(user);
        } else {
            user.setAdresses(user.getAdresses().stream().map(ad -> {
                if (ad.getId() == adresse.getId()) {
                    ad = adresse;
                }
                return ad;
            }).collect(Collectors.toList()));
        }
        session.setAttribute("user", user);
        return "redirect:/tableauadresseslivraison";
    }

    @GetMapping("{email}")
    public String mesdonnees(@PathVariable String email, @RequestParam String action, Model model) {
        Optional<User> user = userService.findByEmail(email);
        if (action.equals("delete")) {

            if (user.isPresent()) {

                userService.delete(email);
            }

            return "redirect:donneespersonnelles";
        }

        if (action.equals("update") && user.isPresent()) {
            model.addAttribute("article", user.get());
            model.addAttribute("action", "/" + email + "?action=update");
            model.addAttribute("fragment", "donneespersonnelles");
        }

        return "index";
    }

    @GetMapping("{id}")
    public String monadresse(@PathVariable Long id, @RequestParam String action, Model model) {
        Optional<Address> adresse = adresseService.findById(id);
        if (action.equals("delete")) {

            if (adresse.isPresent()) {

                adresseService.delete(id);
            }

            return "redirect:adresseslivraison";
        }

        if (action.equals("update") && adresse.isPresent()) {
            model.addAttribute("article", adresse.get());
            model.addAttribute("action", "/" + id + "?action=update");
            model.addAttribute("fragment", "adresseslivraison");
        }

        return "index";
    }

}