package com.kartina.spring.controllers;

import com.kartina.spring.services.OeuvreService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    private final OeuvreService oeuvreService;

    @Autowired
    public IndexController(OeuvreService oeuvreService) {
        this.oeuvreService = oeuvreService;
    }


    @GetMapping("/oeuvres")
    public String oeuvres(Model model) {
        System.out.println(oeuvreService.findAll());
        model.addAttribute("oeuvres", oeuvreService.findAll());
        model.addAttribute("fragment", "oeuvre");
        return "index";
    }

    @GetMapping("/dernierexemplaire")
    public String dernierexemplaire(Model model) {
        model.addAttribute("lastpicture", oeuvreService.lastphotography());
        model.addAttribute("fragment", "dernierexemplaire");
        return "index";
    }

    @GetMapping("/nouveaute")
    public String nouveaute(Model model) {
        model.addAttribute("picture", oeuvreService.newphotography());
        model.addAttribute("fragment", "nouveaute");
        return "index";
    }

    @GetMapping("/photographie")
    public String photographie(Model model) {
        model.addAttribute("photos", oeuvreService.searchphotography());
        model.addAttribute("fragment", "photographie");
        return "index";
    }

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("articles", oeuvreService.findAll());
        model.addAttribute("oeuvres", oeuvreService.caroussel());
        model.addAttribute("image", oeuvreService.quatro());
        model.addAttribute("fragment", "accueil");
        return "index";
    }

    @GetMapping("/mentions_legales")
    public String mentionsLegales(Model model) {
        model.addAttribute("fragment", "mentions_legales");
        return "index";
    }

    @GetMapping("/cgu")
    public String CGU(Model model) {
        model.addAttribute("fragment", "cgu");
        return "index";
    }

    /// a verifier et modifier
    /*
     * @PostMapping("/upload") public BodyBuilder
     * uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException
     * {
     * 
     * System.out.println("Original Image Byte Size - " + file.getBytes().length);
     * ImageModel img = new ImageModel(file.getOriginalFilename(),
     * file.getContentType(), compressBytes(file.getBytes()));
     * imageRepository.save(img); return ResponseEntity.status(HttpStatus.OK); }
     * 
     * @GetMapping(path = { "/get/{imageName}" }) public ImageModel
     * getImage(@PathVariable("imageName") String imageName) throws IOException {
     * 
     * final Optional<ImageModel> retrievedImage =
     * imageRepository.findByName(imageName); ImageModel img = new
     * ImageModel(retrievedImage.get().getName(), retrievedImage.get().getType(),
     * decompressBytes(retrievedImage.get().getPicByte())); return img; }
     */
}
