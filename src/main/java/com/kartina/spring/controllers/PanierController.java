package com.kartina.spring.controllers;

import com.kartina.spring.models.Oeuvre;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Iterator;

@Controller
public class PanierController {

    @GetMapping("/panier")
    public String panier(Model model,
                         HttpSession httpSession,
                         @RequestParam(required = false, defaultValue = "") String action,
                         @RequestParam(required = false, defaultValue = "") Long id) {

        if(action.equals("delete")) {
            return deleteInsideCart(httpSession, id); //Permet de revenir a l'url /panier sans action et delete en param
        }

        checkCartEmpty(httpSession);

        model.addAttribute("fragment", "panier");
        model.addAttribute("panier", httpSession.getAttribute("panier"));

        if (httpSession.getAttribute("panier") != null) {
            model.addAttribute("amount", getTotal(httpSession)[0] * 100); // *100 pour stripe
            model.addAttribute("quantite", getTotal(httpSession)[1].intValue());
        }

        return "index";
    }

    @GetMapping("/panier/checkout")
    public String checkout(Model model) {
        model.addAttribute("fragment", "checkout");

        return "index";
    }

    private String deleteInsideCart(HttpSession httpSession, Long id) {
        ArrayList<Oeuvre> panier = (ArrayList<Oeuvre>)httpSession.getAttribute("panier");
        Iterator<Oeuvre> it = panier.iterator();

        while (it.hasNext()) {
            Oeuvre test = it.next();
            if(test.getId().equals(id)) {
                it.remove();
                break;
            }
        }

        return "redirect:/panier";
    }

    private void checkCartEmpty(HttpSession httpSession) {
        if (httpSession.getAttribute("panier") == null) { return; }
        ArrayList<Oeuvre> panier = (ArrayList<Oeuvre>)httpSession.getAttribute("panier");
        if (panier.isEmpty()) { httpSession.setAttribute("panier", null); }
    }

    private Double[] getTotal(HttpSession httpSession) {
        Double total = 0.00;
        Integer count = 0;

        ArrayList<Oeuvre> panier = (ArrayList<Oeuvre>)httpSession.getAttribute("panier");
        Iterator<Oeuvre> it = panier.iterator();

        while (it.hasNext()) {
            Oeuvre test = it.next();
            total = total + test.getPrice();
            count ++;
        }

        return new Double[]{total, count.doubleValue()};
    }
}
