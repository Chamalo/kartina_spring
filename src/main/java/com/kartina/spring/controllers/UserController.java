package com.kartina.spring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;


import javax.validation.Valid;

import com.kartina.spring.models.User;
import com.kartina.spring.enums.GendertypeEnum;
import com.kartina.spring.enums.RoleEnum;
import com.kartina.spring.models.Address;
import com.kartina.spring.services.AddressService;
import com.kartina.spring.services.UserService;
import com.kartina.spring.utils.PasswordEncoderJava;

@Controller
public class UserController {

    private UserService userService;
    private AddressService adresseService;

    @Autowired
    public UserController(UserService userService, AddressService adresseService) {
            this.adresseService = adresseService;
        this.userService = userService;
    }

    @GetMapping("/creationcompte")
    public String users(Model model
    ) {
        model.addAttribute("adresse", new Address());
        model.addAttribute("user", new User());
        model.addAttribute("GtypeEnumList", GendertypeEnum.values());
        model.addAttribute("fragment", "creationcompte");
        return "index";
    }

    @PostMapping("/creationcompte")
    public String postForm(Model model, 
            @Valid @ModelAttribute(name = "user") User user,
            BindingResult userBinding,
            @Valid @ModelAttribute(name = "adresse") Address adresse,
            BindingResult adresseBinding
            // valid dvt attribute + binding = si formulaire vide ca indique les erreurs
            // dans la console
    // si le formulaire est contrainte de vide (avec le ! dvt), tu px sauvegarder
    ) {

        System.out.println(user.getGtype());
        if (userService.findAll().spliterator().estimateSize() == 0){
            user.setRole(RoleEnum.ADMIN);
        }else{
            user.setRole(RoleEnum.USER);
        }

         if (!userBinding.hasErrors()) {
           if (userService.findByEmail(user.getEmail()).isEmpty()) {
               PasswordEncoderJava pwdEncoder = new PasswordEncoderJava();
                byte[] salt = pwdEncoder.generateSalt();
                byte[] password = pwdEncoder.generatePassword(user.getPwd() ,salt);
                user.setPwdHash(Base64.getEncoder().encodeToString(password));
                
                user.setSalt(Base64.getEncoder().encodeToString(salt));
                this.adresseService.save(adresse);
                user.setAdresses(new ArrayList<>(Arrays.asList(adresse)));
                this.userService.save(user);
                return "redirect:/";
                } else {
                 model.addAttribute("emailExist", "Email already used");
                 }
             }
         

        model.addAttribute("fragment","creationcompte");
        return "index";
    }
}