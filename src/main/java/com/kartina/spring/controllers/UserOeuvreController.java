package com.kartina.spring.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;

import javax.validation.Valid;

import com.kartina.spring.models.Oeuvre;
import com.kartina.spring.services.OeuvreService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/user/oeuvres")
public class UserOeuvreController {

    private final OeuvreService oeuvreService;

    @Autowired
    public UserOeuvreController(OeuvreService oeuvreService) {
        this.oeuvreService = oeuvreService;
    }

    @GetMapping("add")
    public String getCreateOeuvre(Model model, Oeuvre oeuvre){
        model.addAttribute("action", "/add");
        model.addAttribute("fragment", "form");
        return "index";
    }

    @PostMapping("add")
    public String postCreateOeuvre(
            Model model,
            @Valid @ModelAttribute(name = "oeuvre") Oeuvre oeuvre,
            BindingResult oeuvreBinding,
            @RequestParam(name = "image") MultipartFile image,
            RedirectAttributes attributes
    ){
        if (!oeuvreBinding.hasErrors()) {
            boolean isValid = true;
            if (!image.isEmpty() && image.getContentType().equals("image/jpeg")
                    || image.getContentType().equals("image/webp")
                    || image.getContentType().equals("image/heic")
                    || image.getContentType().equals("image/png")
            ) {
                File img = new File("src/main/resources/static/images/" + image.getOriginalFilename());
                try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(img))) {
                    bos.write(image.getBytes());
                    oeuvre.setLink(image.getOriginalFilename());
                } catch (IOException e) {
                    isValid = false;
                    model.addAttribute("action", "/add");
                    model.addAttribute("fragment", "form");
                    model.addAttribute("errorMessage", "Un problème de sauvegarde est survenu");
                }
            }

            if (isValid) {
                oeuvreService.save(oeuvre);
                attributes.addFlashAttribute("message", "L'oeuvre " + oeuvre.getTitle() + " a bien été enregistré");
                return "redirect:/user/oeuvres";
            }
        }
        model.addAttribute("action", "/add");
        model.addAttribute("fragment", "form");
        return "index";
    }

    @GetMapping("{ref}")
    public String oneOeuvre(
            @PathVariable Long ref,
            @RequestParam String action,
            Model model
    ) {
        Optional<Oeuvre> oeuvre = oeuvreService.findById(ref);
        if (action.equals("delete")) {

            if (oeuvre.isPresent()) {
                oeuvreService.delete(ref);
            }

            return "redirect:/user/oeuvres";
        }

        if (action.equals("update") && oeuvre.isPresent()) {
            model.addAttribute("oeuvre", oeuvre.get());
            model.addAttribute("action", "/" + ref + "?action=update");
            model.addAttribute("fragment", "form");
        }

        return "index";
    }

    @PostMapping("{ref}")
    public String update(
            @Valid @ModelAttribute(name = "oeuvre") Oeuvre oeuvre,
            BindingResult oeuvreBinding,
            @PathVariable Long ref,
            @RequestParam String action,
            @RequestParam(name = "image") MultipartFile image,
            Model model,
            RedirectAttributes attributes
    ) {

        if (!oeuvreBinding.hasErrors()) {


            boolean isValid = true;

            if (!image.isEmpty() && image.getContentType().equals("image/jpeg")
                    || image.getContentType().equals("image/webp")
                    || image.getContentType().equals("image/heic")
                    || image.getContentType().equals("image/png")
            ) {

                File img = new File("src/main/resources/static/images/" + image.getOriginalFilename());
                try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(img))) {
                    bos.write(image.getBytes());
                    if (new File("src/main/resources/static/images/" + oeuvre.getLink()).delete()) {
                        oeuvre.setLink(image.getOriginalFilename());
                    }
                } catch (IOException e) {
                    isValid = false;
                    model.addAttribute("action", "/" + ref + "?action=update");
                    model.addAttribute("fragment", "form");
                    model.addAttribute("errorMessage", "Un problème de sauvegarde est survenu");
                }
            }

            if (isValid) {
                oeuvreService.save(oeuvre);
                attributes.addFlashAttribute("message", "L'oeuvre " + ref + " a bien été mis à jour");
                return "redirect:/user/oeuvres";
            }
        }
        model.addAttribute("action", "/" + ref + "?action=update");
        model.addAttribute("fragment", "form");
        return "index";
    }


    
}
