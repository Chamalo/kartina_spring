package com.kartina.spring.controllers;

import com.kartina.spring.models.ContactModel;
import com.kartina.spring.services.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;


@Controller
public class AideController {

    private final ContactService contactService;
    private final JavaMailSender emailSender;

    @Autowired
    public AideController(ContactService contactService, JavaMailSender emailSender) {
        this.contactService = contactService;
        this.emailSender = emailSender;
    }

    @GetMapping("/aide")
    public String aide(Model model, ContactModel contact) {
        model.addAttribute("fragment", "aide");
        model.addAttribute("contact", contact);
        return "index";
    }

    @PostMapping("/aide")
    public String aideSend(Model model,
                           @Valid @ModelAttribute(name = "contact") ContactModel contact,
                           BindingResult bindingResult) {

        if(!bindingResult.hasErrors()) {
            MimeMessage messageToSend = this.emailSender.createMimeMessage();

            try {
                MimeMessageHelper helper = new MimeMessageHelper(messageToSend, true);

                String messageWithBr = contact.getMessage().replaceAll("(\r\n|\n)", "<br>"); //Set Linebreak

                String htmlMessage = getHtmlMessage(contact, messageWithBr);

                try {
                    messageToSend.setContent(htmlMessage, "text/html");

                    helper.setTo(contact.getMail());
                    helper.setSubject(contact.getSubject());
                } catch (MessagingException e) {
                    e.printStackTrace();

                    model.addAttribute("statusMessage", "Error sending mail, please try again (content)");
                    model.addAttribute("fragment", "aide");

                    return "index";
                }
            } catch (MessagingException e) {
                e.printStackTrace();

                model.addAttribute("statusMessage", "Error sending mail, please try again (helper)");
                model.addAttribute("fragment", "aide");

                return "index";
            }

            try {
                this.emailSender.send(messageToSend);

                this.contactService.save(contact);
            } catch (MailException e) {
                e.printStackTrace();

                model.addAttribute("statusMessage", "Error sending mail, please try again");
                model.addAttribute("fragment", "aide");

                return "index";
            }
        }

        model.addAttribute("statusMessage", "Email send, you will get a copy soon, check your mail/spam");
        return "index";
    }

    private String getHtmlMessage(ContactModel contact, String messageWithBr) {
        return "<br><br><img src='https://zupimages.net/up/20/38/4ho3.png'><br><br>" +
                "<h3>Dear " + contact.getCiv() + " " + contact.getLastname().toUpperCase() + "<i> " + contact.getFirstname() + "</i> (phone: " + contact.getPhone()+ ")</h3>" +
                "This is the copy of your request : <br><br><br> <i>\"" + messageWithBr +
                "\"</i> <br><br><br>We will answer you, as fast as possible." +
                "<br><br>Thanks for your patience<br><br><b>Kartina Team</b>";
    }
}
