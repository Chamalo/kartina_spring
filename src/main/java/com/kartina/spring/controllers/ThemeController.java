package com.kartina.spring.controllers;

import java.util.Collection;

import com.kartina.spring.models.Theme;
import com.kartina.spring.services.OeuvreService;
import com.kartina.spring.services.ThemeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ThemeController {
    

    private final OeuvreService oeuvreService;
    private final ThemeService themeService;

    @Autowired
    public ThemeController (OeuvreService oeuvreService,ThemeService themeService) {
        this.oeuvreService = oeuvreService;
        this.themeService = themeService;
    }

    /* @GetMapping("/voyage")
    public String voyage(
        Model model,
        @RequestParam(defaultValue = "") String typeTheme) {
        Collection<Theme> travels = this.themeService.findByTypeTheme("voyageAS/voyageAF/voyageAM/voyageEU"); */
        /* Collection<Theme> travaf = this.themeService.findByTypeTheme("voyageAF");
        Collection<Theme> travam = this.themeService.findByTypeTheme("voyageAM");
        Collection<Theme> traveu = this.themeService.findByTypeTheme("voyageEU");
        Collection<Theme> travedg = this.themeService.findByTypeTheme("voyageDG");
        Collection<Theme> traval = this.themeService.findByTypeTheme("voyageAL"); */
      //  model.addAttribute("themes", travels);
        /* model.addAttribute("themes", travaf);
        model.addAttribute("themes", travam);
        model.addAttribute("themes", traveu);
        model.addAttribute("themes", travedg);
        model.addAttribute("themes", traval); */
       // model.addAttribute("fragment", "voyage");
       // return "index";
        //}

        @GetMapping("/voyage")
        public String voyage(
            Model model,
            @RequestParam(defaultValue = "") String typeTheme) {
            Collection<Theme> travels = this.themeService.findAll();
            Collection<Theme> traveu = this.themeService.findAll();
            Collection<Theme> travaf = this.themeService.findAll();
            Collection<Theme> travam = this.themeService.findAll();
            Collection<Theme> travedg = this.themeService.findAll();
            Collection<Theme> traval = this.themeService.findAll();
            model.addAttribute("travels", themeService.findByTypeTheme("voyageAS"));
            model.addAttribute("traveu", themeService.findByTypeTheme("voyageEU"));
            model.addAttribute("travaf", themeService.findByTypeTheme("voyageAF"));
            model.addAttribute("travam", themeService.findByTypeTheme("voyageAM"));
            model.addAttribute("travegf", themeService.findByTypeTheme("voyageGF"));
            model.addAttribute("traval", themeService.findByTypeTheme("voyageAL"));
            model.addAttribute("fragment", "voyage");
            return "index";
        } 



    @GetMapping("/noirblanc")
    public String noirblanc(
        Model model,
        @RequestParam(defaultValue = "") String typeTheme) {
        Collection<Theme> themes = this.themeService.findByTypeTheme("noirblanc");
        System.out.println(themes);
        model.addAttribute("themes", themes);
        model.addAttribute("fragment", "noirblanc");
        return "index";
        }
}