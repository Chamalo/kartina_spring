package com.kartina.spring.controllers;

import java.util.Base64;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.kartina.spring.models.User;
import com.kartina.spring.services.UserService;
import com.kartina.spring.utils.PasswordEncoderJava;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {

    private UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String login(Model model, User user) {
        model.addAttribute("fragment", "login");
        model.addAttribute("user", user);
        return "index";
    }

    @PostMapping("/login")
    public String loginUser(
                            Model model,
                            @Valid @ModelAttribute(name = "user") User user,
                            BindingResult userBinding,
                            HttpSession session
    ){
        if(!userBinding.hasErrors()) {
            Optional<User> userFind = userService.findByEmail(user.getEmail());
            if(userFind.isPresent()) {
                PasswordEncoderJava pwdEncoder = new PasswordEncoderJava();
                byte[] pwd = pwdEncoder.generatePassword(user.getPwd(), Base64.getDecoder().decode(userFind.get().getSalt()));
                if(Base64.getEncoder().encodeToString(pwd).equals(userFind.get().getPwdHash())) {
                    System.out.println(userFind.get().getAdresses().size());
                    session.setAttribute("user", userFind.get());
                    return "redirect:/";
                }
            }
            model.addAttribute("loginError", "Wrong email or password");
        }
        model.addAttribute("fragment", "login");
        return "index";
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        session.setAttribute("user", null); //Si tu veux garder en mémoire un panier par exemple
        return "redirect:/";
    }

    @GetMapping("/forget")
    public String forgetpwd(Model model, User user) {
        model.addAttribute("user", user);
        model.addAttribute("fragment", "forgetpwd");
        return "index";
    }
}





